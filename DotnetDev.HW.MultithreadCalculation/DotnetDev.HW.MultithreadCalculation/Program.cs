﻿using System;
using System.Collections.Generic;
using DotnetDev.HW.MultithreadCalculation.Calculators;

namespace DotnetDev.HW.MultithreadCalculation
{
    internal static class Program
    {
        private static readonly IReadOnlyCollection<CalculatorBase> _calculators = new[]
        {
            // Здесь перечислены все калькуляторы в программе
            new SimpleCalculator(new RandomCollection(100_000))
        };
        
        private static void Main(string[] args)
        {
            foreach (var calculator in _calculators)
            {
                Console.WriteLine(calculator.Total());
                Console.WriteLine(calculator.Elapsed);
            }
        }
    }
}