using System.Collections.Generic;
using System.Linq;

namespace DotnetDev.HW.MultithreadCalculation.Calculators
{
    public class SimpleCalculator : CalculatorBase
    {
        public SimpleCalculator(IEnumerable<int> collection)
            : base(collection)
        {
        }

        protected override long TotalInternal()
        {
            return Collection.Sum();
        }
    }
}