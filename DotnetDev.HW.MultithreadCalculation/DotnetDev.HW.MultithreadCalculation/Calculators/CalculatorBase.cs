using System.Collections.Generic;
using System.Diagnostics;

namespace DotnetDev.HW.MultithreadCalculation.Calculators
{
    public abstract class CalculatorBase
    {
        public long Elapsed => _stopwatch.ElapsedMilliseconds;

        protected IEnumerable<int> Collection { get; }

        private readonly Stopwatch _stopwatch;
        
        protected CalculatorBase(IEnumerable<int> collection)
        {
            Collection = collection;
            _stopwatch = new Stopwatch();
        }

        public long Total()
        {
            _stopwatch.Start();
            var result = TotalInternal();
            _stopwatch.Stop();
            return result;
        }

        protected abstract long TotalInternal();
    }
}