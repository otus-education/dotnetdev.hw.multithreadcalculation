using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DotnetDev.HW.MultithreadCalculation
{
    public class RandomCollection : IReadOnlyCollection<int>
    {
        private readonly IReadOnlyCollection<int> _collection;

        public RandomCollection(int maxCount)
        {
            _collection = Create(maxCount).ToArray();
        }

        public IEnumerator<int> GetEnumerator() => _collection.GetEnumerator();
        
        private IEnumerable<int> Create(int maxCount)
        {
            for (int i = 0; i < maxCount; i++)
            {
                yield return RandomNumber(i);
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private int RandomNumber(int seed)
        {
            throw new NotImplementedException();
        }

        public int Count => _collection.Count;
    }
}